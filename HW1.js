var mode = 0; //0 for draw 1 for erase
var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var startx, starty;
var snapshot;
var snap_index = -1;
var snap_cnt = -1;
var snap = [];
var img_flag = 0;
var fontsize = 8;
var fontshape = "Ariel";
var hasInput = false;
let drawcolor = "black";
let drawwidth = "50";
let isdraw = false;
let imgInput = document.getElementById("upload");
canvas.width = window.innerWidth - 100;
canvas.height = 500;
ctx.font = "8px Ariel";
font = "8px Ariel";

canvas.addEventListener("touchstart", start, false);
canvas.addEventListener("mousedown", start, false);

canvas.addEventListener("touchmove", dothing, false);
canvas.addEventListener("mousemove", dothing, false);

canvas.addEventListener("mouseup", leave, false);
canvas.addEventListener("mouseout", leave, false);
canvas.addEventListener("touchend", leave, false);

imgInput.addEventListener("change", uploadimg, false);
document.getElementById("pen").style.backgroundColor =
  mode === 0 ? "#ffffff" : "#f8e1c6";

var und = document.getElementById("undo");
und.addEventListener("mouseenter", undcol, false);
und.addEventListener("mouseleave", undcol_back, false);

function undcol() {
  und.style.background = "#b99e6c";
}
function undcol_back() {
  und.style.background = "#fabd4c";
}
var red = document.getElementById("redo");
red.addEventListener("mouseenter", redcol, false);
red.addEventListener("mouseleave", redcol_back, false);
function redcol() {
  red.style.background = "#b99e6c";
}
function redcol_back() {
  red.style.background = "#fabd4c";
}
var clr = document.getElementById("clear");
clr.addEventListener("mouseenter", clrcol, false);
clr.addEventListener("mouseleave", clrcol_back, false);
function clrcol() {
  clr.style.background = "#b99e6c";
}
function clrcol_back() {
  clr.style.background = "#fabd4c";
}

function download() {
  var download = document.getElementById("download");
  var image = canvas
    .toDataURL("image/png")
    .replace("image/png", "image/octet-stream");
  download.setAttribute("href", image);
}

function uploadimg(event) {
  if (event.target.files) {
    let imageFile = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(imageFile);
    reader.onloadend = function (e) {
      var myImage = new Image();
      myImage.src = e.target.result;
      myImage.onload = function (ev) {
        canvas.width = myImage.width;
        canvas.height = myImage.height;
        ctx.drawImage(myImage, 0, 0);
        takeSnapshot();
        let imgData = canvas.toDataURL("image/jpeg", 0.75);
        snap = [];
        snap.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
        snap_cnt = 0;
        snap_index = 0;
      };
    };
  }
  img_flag = 1;
}
function dothing(event) {
  switch (mode) {
    case 0: {
      ctx.globalCompositeOperation = "source-over";
      draw(
        event.clientX - canvas.offsetLeft - 8,
        event.clientY - canvas.offsetTop - 8
      );
      break;
    }
    case 1: {
      ctx.globalCompositeOperation = "destination-out";
      erase(
        event.clientX - canvas.offsetLeft - 8,
        event.clientY - canvas.offsetTop - 8
      );
      break;
    }
    case 2: {
      restoreSnapshot();
      ctx.globalCompositeOperation = "source-over";
      triangle(
        event.clientX - canvas.offsetLeft - 8,
        event.clientY - canvas.offsetTop - 8
      );
      break;
    }
    case 3: {
      restoreSnapshot();
      ctx.globalCompositeOperation = "source-over";
      circle(
        event.clientX - canvas.offsetLeft - 8,
        event.clientY - canvas.offsetTop - 8
      );
      break;
    }
    case 4: {
      restoreSnapshot();
      ctx.globalCompositeOperation = "source-over";
      rectangle(
        event.clientX - canvas.offsetLeft - 8,
        event.clientY - canvas.offsetTop - 8
      );
      break;
    }
    case 5: {
      ctx.globalCompositeOperation = "source-over";
      canvas.onclick = function (event) {
        if (!hasInput && mode == 5) addInput(event.clientX, event.clientY);
      };
      canvas.style.cursor = "text";
      break;
    }
  }
  event.preventDefault();
}
function btncolor() {
  document.getElementById("pen").style.backgroundColor =
    mode == 0 ? "#ffffff" : "#f8e1c6";
  document.getElementById("eraser").style.backgroundColor =
    mode == 1 ? "#ffffff" : "#f8e1c6";
  document.getElementById("tri").style.backgroundColor =
    mode == 2 ? "#ffffff" : "#f8e1c6";
  document.getElementById("cir").style.backgroundColor =
    mode == 3 ? "#ffffff" : "#f8e1c6";
  document.getElementById("sqr").style.backgroundColor =
    mode == 4 ? "#ffffff" : "#f8e1c6";
  document.getElementById("text").style.backgroundColor =
    mode == 5 ? "#ffffff" : "#f8e1c6";
}
function start(event) {
  isdraw = true;
  startx = event.clientX - canvas.offsetLeft - 8;
  starty = event.clientY - canvas.offsetTop - 8;
  ctx.beginPath();
  ctx.moveTo(
    event.clientX - canvas.offsetLeft - 8,
    event.clientY - canvas.offsetTop - 8
  );
  takeSnapshot();
}

function undo() {
  if (snap_index > 0) {
    ctx.putImageData(snap[--snap_index], 0, 0);
    console.log(snap_index);
  } else if (snap_index == 0 && img_flag != 1) {
    snap_index--;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }
  takeSnapshot();
}

function redo() {
  if (snap_index < snap_cnt) {
    snap_index++;
    ctx.putImageData(snap[snap_index], 0, 0);
  }
  takeSnapshot();
}

function takeSnapshot() {
  snapshot = ctx.getImageData(0, 0, canvas.width, canvas.height);
}

function draw(X, Y) {
  canvas.style.cursor = "url('./pen_cur.png')4 30 ,default";
  if (isdraw) {
    ctx.lineTo(X, Y);
    ctx.strokeStyle = drawcolor;
    ctx.lineWidth = drawwidth;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";
    ctx.stroke();
  }
}
function erase(X, Y) {
  canvas.style.cursor = "url('./eraser_cur.png')4 30 ,default";
  if (isdraw) {
    ctx.lineTo(X, Y);
    ctx.lineWidth = drawwidth;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";
    ctx.stroke();
  }
}
function triangle(X, Y) {
  canvas.style.cursor = "url('./tri_cur.png')4 30 ,default";
  if (isdraw) {
    ctx.beginPath();
    ctx.lineWidth = drawwidth;
    ctx.strokeStyle = drawcolor;
    ctx.moveTo((startx + X) / 2, starty);
    ctx.lineTo(startx, Y);
    ctx.lineTo(X, Y);
    ctx.closePath();
    ctx.stroke();
  }
}
function restoreSnapshot() {
  ctx.putImageData(snapshot, 0, 0);
}
function circle(X, Y) {
  canvas.style.cursor = "url('./cir_cur.png')4 30 ,default";
  if (isdraw) {
    var radius = Math.sqrt(Math.pow(startx - X, 2) + Math.pow(starty - Y, 2));
    ctx.beginPath();
    ctx.lineWidth = drawwidth;
    ctx.strokeStyle = drawcolor;
    ctx.arc((startx + X) / 2, (starty + Y) / 2, radius, 0, 2 * Math.PI, true);
    ctx.stroke();
  }
}
function rectangle(X, Y) {
  canvas.style.cursor = "url('./sqr_cur.png')4 30 ,default";
  if (isdraw) {
    ctx.beginPath();
    ctx.lineWidth = drawwidth;
    ctx.strokeStyle = drawcolor;
    ctx.moveTo(startx, starty);
    ctx.lineTo(startx, Y);
    ctx.lineTo(X, Y);
    ctx.lineTo(X, starty);
    ctx.closePath();

    ctx.stroke();
  }
}
function leave(event) {
  if (isdraw) {
    ctx.stroke();
    ctx.closePath();
    takeSnapshot();
    for (var i = 0; i < snap_cnt - snap_index; i++) {
      snap.pop();
    }
    snap.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    snap_index++;
    snap_cnt = snap_index;
  }
  console.log(snap_index, snap_cnt);
  isdraw = false;
  event.preventDefault();
}
function clearr() {
  var con = confirm("Want to reset?");
  if (con) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    for (var i = 0; i < snap_cnt - snap_index; i++) {
      snap.pop();
    }
    takeSnapshot();
    snap.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    snap_index++;

    snap_cnt = snap_index;
  }
}

function addInput(x, y) {
  var input = document.createElement("input");
  input.type = "text";
  input.style.position = "fixed";
  input.style.left = x + "px";
  input.style.top = y + "px";
  input.onkeydown = handleEnter;
  document.body.appendChild(input);
  input.focus();
  hasInput = true;
}

function handleEnter(event) {
  var keyCode = event.keyCode;
  var x = parseInt(this.style.left, 10);
  var y = parseInt(this.style.top, 10);
  if (keyCode === 13) {
    ctx.font = fontsize + "px " + fontshape;
    ctx.fillText(this.value, x - canvas.offsetLeft, y - canvas.offsetTop);
    document.body.removeChild(this);
    hasInput = false;
    takeSnapshot();
    for (var i = 0; i < snap_cnt - snap_index; i++) {
      snap.pop();
    }
    snap.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    snap_index++;
    snap_cnt = snap_index;
  }
}
function changesize() {
  fontsize = document.getElementById("fontsizee").value;
}
function changeshape() {
  fontshape = document.getElementById("fontshapee").value;
}
