# Software Studio 2021 Spring

## Assignment 01 Web Canvas

### Scoring

| **Basic components** | **Score** | **Check** |
| :------------------- | :-------: | :-------: |
| Basic control tools  |    30%    |     Y     |
| Text input           |    10%    |     Y     |
| Cursor icon          |    10%    |     Y     |
| Refresh button       |    10%    |     Y     |

| **Advanced tools**     | **Score** | **Check** |
| :--------------------- | :-------: | :-------: |
| Different brush shapes |    15%    |     Y     |
| Un/Re-do button        |    10%    |     Y     |
| Image tool             |    5%     |     Y     |
| Download               |    5%     |     Y     |

| **Other useful widgets** | **Score** | **Check** |
| :----------------------- | :-------: | :-------: |
| Name of widgets          |   1~5%    |     N     |

---

### How to use

![](https://i.imgur.com/12dyeZE.png)

This website allows you to paint or type content on it. By choosing different tools on tool bar, you can add varios color to your works. Bynow we got eraser pen and different brushes like triangle , cirle, rectangle. You can alse type some text on your work.

### Function description

![](https://i.imgur.com/wUfZvlS.png)

We got different tools the reach different effect, the currently choosed tool's icon background will be changed to white.

![](https://i.imgur.com/CMcR50z.png)

There is also some convenient tools like redo, undo, or just clear the whole page.

![](https://i.imgur.com/ZGJfkgI.png)

If you want to save your masterpiece or upload image, there is also buttons to do these function.

![](https://i.imgur.com/WosluPI.png)

You can also change the width and color of your pen, and change the font shape and your font size as you wish.

![](https://i.imgur.com/lBBoKBF.png)

When the mouse is on button's icon, the background color will also change darker that seems like you are pressing the button.

### Gitlab page link

    https://108062129.gitlab.io/AS_01_WebCanvas

<style>
table th{
    width: 100%;
}
</style>
